package main

import (
	"bufio"
	"context"
	"log"
	"os"
	"strings"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatal(err)
	}

	ch, err := conn.Channel()
	if err != nil {
		log.Fatal(err)
	}

	err = ch.ExchangeDeclare(
		"prizon_wizard",
		"direct",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		msg := strings.Split(scanner.Text(), " ")
		if msg[0] == "white" {
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()
			ch.PublishWithContext(
				ctx,
				"prizon_wizard",
				"white-power",
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        []byte(msg[1]),
				})
		} else {
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()
			ch.PublishWithContext(
				ctx,
				"prizon_wizard",
				"black-dickheads",
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body:        []byte(msg[1]),
				})
		}
	}
}

/*
func main() {
	// connect to rabbit
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to rabbitmq")

	// create channel
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	// declare queue
	q, err := ch.QueueDeclare(
		"hello", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	// send to queue
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	body := "Hello World!"
	err = ch.PublishWithContext(ctx,
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	failOnError(err, "Failed to publish a message")
	log.Printf(" [x] Sent %s\n", body)

	defer conn.Close()
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}
*/
